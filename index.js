// funzione cambio colore titolo
let titolo = document.getElementById('titolo');

    function changecolor(tag,colore){
        tag.style.color=colore;
    }
    
    document.getElementById('btn_giallo').addEventListener('click', changecolor.bind(null, titolo, 'yellow'));
    document.getElementById('btn_blu').addEventListener('click', changecolor.bind(null, titolo, 'blue'));
// gestione form
    let nome = document.getElementById('nome');
    let cognome = document.getElementById('cognome');
    let email = document.getElementById('email');
    let matricola = document.getElementById('matricola');
    let residenza = document.getElementById('residenza');
    let telefono = document.getElementById('telefono');
    let areatestuale = document.getElementById('areatestuale');
    // funzione che gestisce il reset
    function reset(){
        nome.value="";
        cognome.value="";
        email.value="";
        matricola.value="";
        residenza.value="";
        telefono.value="";
        areatestuale.value="";
    }
    // funzione che gestisce l'invio dei dati e controlla eventuali errori
    function submit(){
    if (nome.value.length == 0){
        alert("inserire nome");
    }
    else if(cognome.value.length == 0){
        alert("inserire cognome");
    }
    else if(matricola.value.length == 0){
        alert("inserire matricola");
    }
    else
        nome.value="";
        cognome.value="";
        matricola.value="";
        residenza.value="";
        telefono.value="";
        areatestuale.value="";
    }

    let btninvio = document.getElementById('btn_invio').addEventListener('click', submit);
    let btnreset = document.getElementById('btn_reset').addEventListener('click', reset);